<?php
/**
 * Created by PhpStorm.
 * User: develalfy
 * Date: 1/17/17
 * Time: 5:36 PM
 */

namespace App\Core\Adapters;


use Exception;
use Illuminate\Support\Facades\Mail;

class MailerAdapter
{
    public function sendEmail($data)
    {
        try {
            Mail::raw($data['body'], function ($message) use ($data) {
                $message->from(env('EMAIL_ADDRESS'), env('MAIL_SENDER_NAME'));
                $message->to($data['to'])->subject($data['title']);
                $message->attach(public_path('AshrafElalfy_CV.pdf'));
            });
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}