<?php
/**
 * Created by PhpStorm.
 * User: develalfy
 * Date: 1/17/17
 * Time: 1:54 AM
 */

namespace App\Http\Controllers;


use App\Core\Adapters\MailerAdapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $defaultTitle = "PHP / Backend Developer";
        $defaultBody = <<<EOT
Hello,
I am Ashraf Elalfi,
I have a PHP experience for about more than 2 years with a good experience in frameworks like Laravel.

You can find my CV attached below

My linkedIn:-
https://eg.linkedin.com/in/develalfy

Thank you,
EOT;

        return view('mailer', compact('defaultBody', 'defaultTitle'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send(Request $request)
    {
        $data = array(
            'title' => $request->get('title'),
            'body' => $request->get('body'),
        );

        $emailsArr = explode(PHP_EOL, $request->get('to'));
        $mailer = new MailerAdapter();
        foreach ($emailsArr as $email) {
            $this->timeLimits(2, 30);
            $email = $this->cleanEmail($email);

            if ($email === '') {
                continue;
            }
            $data['to'] = $email;
            $mailer->sendEmail($data);
        }


        Session::flash('flash_message', 'E-mails have been sent');
        return redirect()->back();
    }

    /**
     * @param $email
     * @return mixed
     */
    private function cleanEmail($email)
    {
        $email = str_replace(' ', '', $email);
        $email = str_replace("\r", '', $email);

        return $email;
    }

    private function timeLimits($sleep, $exec_limit)
    {
        sleep($sleep);
        set_time_limit($exec_limit);
    }
}