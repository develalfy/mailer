@extends('layouts.app')

@section('title', 'Mailer')

@section('content')
	<div class="container">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{ Session::get('flash_message') }}</div>
		@endif
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading"><code>DevElalfy</code> - Mailer</div>

					<div class="panel-body">
						<form class="form-horizontal" role="form" method="POST" action="{{ url('/mailer') }}">
							{{ csrf_field() }}

							<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
								<label for="title" class="col-md-4 control-label">Title</label>

								<div class="col-md-6">
									<input id="title" type="text" class="form-control" name="title"
									       value="{{ old('title')?: $defaultTitle }}">

									@if ($errors->has('title'))
										<span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
								<label for="body" class="col-md-4 control-label">Body</label>

								<div class="col-md-6">
									<textarea id="body" class="form-control" name="body"
									          rows="10">{{ (old('body')) ?: $defaultBody }}</textarea>

									@if ($errors->has('body'))
										<span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<label for="to" class="col-md-4 control-label"></label>

								<div class="col-md-6">
									<code>* E-mail for every line</code>
								</div>
							</div>

							<div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
								<label for="to" class="col-md-4 control-label">To</label>

								<div class="col-md-6">
									<textarea id="to" class="form-control" name="to"
									          rows="10">{{ old('to') }}</textarea>

									@if ($errors->has('to'))
										<span class="help-block">
                                        <strong>{{ $errors->first('to') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<div class="">
										<button type="submit" class="btn btn-primary">
											<i class="fa fa-btn fa-sign-in"></i> Send
										</button>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
